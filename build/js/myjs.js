// JavaScript Document

// Nicht verwendet!
$("#vegas-container").vegas({
	delay: 17000,
	cover: true,
	timer: true,
	overlay: true,
	shuffle: true,
	transition: 'fade',
	transitionDuration: 2000,
	animation: [ 'kenburns' ],
	slides: [
		{ src: 'assets/img/luftbild-tegernsee.png' },
		{ src: 'assets/img/tegernsee-berge.png' }
	]
});